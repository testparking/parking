package com.test.parking.controllers;


import com.test.parking.controllers.request.entranceRequest;
import com.test.parking.controllers.request.updateRequest;
import com.test.parking.controllers.response.entranceResponse;
import com.test.parking.controllers.response.outResponse;
import com.test.parking.controllers.response.profitResponse;
import com.test.parking.services.parkingService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/api")
public class ParkingController {

    private final parkingService pService;

    public ParkingController(parkingService pService) {
        this.pService = pService;
    }

    @GetMapping
    @ApiOperation("Get all bills in the parking")
    @ApiResponse(code=200, message = "OK")
    public Page<outResponse> getAll(@RequestParam(required = false, value = "dateIn") String dIn,
                                    @RequestParam(required = false, value = "dateOut") String dOut,
                                    @RequestParam int page,
                                    @RequestParam int size) {

        PageRequest requestPage = PageRequest.of(page, size, Sort.Direction.ASC, "idBill");
        LocalDateTime dateIn = LocalDateTime.parse(dIn);
        LocalDateTime dateOut = LocalDateTime.parse(dOut);

        return pService.getAll(requestPage, dateIn, dateOut);
    }

    @GetMapping("/getById/{idBill}")
    @ApiOperation("Search a register with an ID")
    public outResponse getById(@PathVariable(name = "idBill") Integer idBill){
        return pService.getById(idBill);
    }

    @PostMapping("/in")
    @ApiOperation("Register when a vehicle enter")
    public entranceResponse in(@RequestBody entranceRequest request) throws Exception {

        log.info("A vehicle enters the parking lot with the request: {}",request);
        return pService.entrance(request);

    }

    @GetMapping("/out/{idBill}")
    public outResponse out(@PathVariable(name = "idBill") Integer idBill) throws Exception {

        log.info("the vehicle with bill {} is get out",idBill);
        return pService.out(idBill);

    }

    @GetMapping("/profits")
    public profitResponse profits(@RequestParam(required = false, value = "dateIn") String dIn,
                                  @RequestParam(required = false, value = "dateOut") String dOut) {

        LocalDateTime dateIn = LocalDateTime.parse(dIn);
        LocalDateTime dateOut = LocalDateTime.parse(dOut);

        return pService.profits(dateIn, dateOut);

    }

    @PostMapping("/update/{idBill}")
    public outResponse update(@PathVariable(name = "idBill") Integer idBill,
                              @RequestBody updateRequest request){

        log.info("The bill {} is update with {}",idBill, request);

        return pService.update(idBill, request);
    }

    @GetMapping("/delete/{idBill}")
    public void delete(@PathVariable(name = "idBill") Integer idBill){
        pService.delete(idBill);
    }

}
