package com.test.parking.controllers.request;

import lombok.Data;

@Data
public class entranceRequest {

    private String plate;
    private Integer vehicleType;
    private String vehicleClass;
    private Integer lot;

}
