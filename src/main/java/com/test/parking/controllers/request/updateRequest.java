package com.test.parking.controllers.request;

import lombok.Data;

@Data
public class updateRequest {

    private String dateIn;
    private String dateOut;
    private int vehicleType;
    private String vehicleClass;

}
