package com.test.parking.controllers.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.test.parking.domains.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class entranceResponse {

    private int idBill;
    private String plate;
    private int vehicleType;
    private String vehicleClass;
    private int lot;
    private LocalDateTime dateIn;

    public entranceResponse(Vehicle vehicle, Bill bill) {
        this.idBill = bill.getIdBill();
        this.plate = vehicle.getPlate();
        this.vehicleType = vehicle.getVehicleType().getIdVehicleType();
        this.vehicleClass = vehicle.getVehicleClass();
        this.lot = bill.getLot().getIdLot();
        this.dateIn = bill.getDateIn();
    }

}
