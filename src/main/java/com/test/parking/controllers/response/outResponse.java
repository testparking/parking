package com.test.parking.controllers.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.test.parking.domains.Bill;
import com.test.parking.domains.Vehicle;
import com.test.parking.domains.VehicleBill;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class outResponse {

    private int idBill;
    private String plate;
    private String vehicleType;
    private String vehicleClass;
    private String lot;
    private LocalDateTime dateIn;
    private LocalDateTime dateOut;
    private Double discount;
    private Double total;

    public outResponse(VehicleBill vehicleBill, Bill bill) {
        this.idBill = bill.getIdBill();
        this.plate = vehicleBill.getVehicle().getPlate();
        this.vehicleType = vehicleBill.getVehicle().getVehicleType().getNameVehicle();
        this.vehicleClass = vehicleBill.getVehicle().getVehicleClass();
        this.lot = bill.getLot().getNameLot();
        this.dateIn = bill.getDateIn();
        this.dateOut = bill.getDateOut();
        this.discount = bill.getDiscount();
        this.total = bill.getTotal();
    }

}
