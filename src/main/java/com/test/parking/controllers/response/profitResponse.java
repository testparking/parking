package com.test.parking.controllers.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.test.parking.domains.Bill;
import com.test.parking.domains.Vehicle;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class profitResponse {

    private int amount;
    private Double profits;

    public profitResponse(Integer amount, Double profits) {
        this.amount = amount;
        this.profits = profits;
    }

}
