package com.test.parking.domains;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.lang.annotation.Documented;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "bill")
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_bill")
    private Integer idBill;

    @ColumnDefault("false")
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "id_lot")
    private Lot lot;

    private Double discount;

    @Column(name = "date_in")
    private LocalDateTime dateIn;

    @Column(name = "date_out")
    private LocalDateTime dateOut;

    private Double total;

}
