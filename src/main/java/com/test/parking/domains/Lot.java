package com.test.parking.domains;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Data
@Entity
@Table(name = "lot")
public class Lot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_lot")
    private Integer idLot;

    @ColumnDefault("false")
    private boolean deleted;

    @Column(name = "name_lot")
    private String nameLot;

    @ManyToOne
    @JoinColumn(name = "id_vehicle_type")
    private VehicleType VehicleType;

    @ManyToOne
    @JoinColumn(name = "id_status")
    private Status status;
}
