package com.test.parking.domains;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Data
@Entity
@Table(name = "status")
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_status")
    private Integer id_Status;

    @ColumnDefault("false")
    private boolean deleted;

    @Column(name = "name_status")
    private String nameStatus;

}
