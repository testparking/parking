package com.test.parking.domains;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Data
@Entity
@Table(name = "vehicle")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_vehicle")
    private Integer idVehicle;

    @ColumnDefault("false")
    private boolean deleted;

    private String plate;

    @Column(name = "vehicle_class")
    private String vehicleClass;

    @ManyToOne
    @JoinColumn(name = "id_vehicle_type")
    private VehicleType vehicleType;

}
