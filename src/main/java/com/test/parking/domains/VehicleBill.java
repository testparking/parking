package com.test.parking.domains;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Data
@Entity
@Table(name = "vehicle_bill")
public class VehicleBill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_vehicle_bill")
    private Integer idVehicleBill;

    @ColumnDefault("false")
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "id_vehicle")
    private Vehicle vehicle;

    @ManyToOne
    @JoinColumn(name = "id_bill")
    private Bill bill;

}
