package com.test.parking.domains;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Data
@Entity
@Table(name = "vehicle_type")
public class VehicleType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_vehicle_type")
    private Integer idVehicleType;

    @ColumnDefault("false")
    private boolean deleted;

    @Column(name = "name_vehicle")
    private String nameVehicle;

}
