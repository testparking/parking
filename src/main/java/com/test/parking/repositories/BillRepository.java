package com.test.parking.repositories;

import com.test.parking.domains.Bill;
import com.test.parking.repositories.commons.SoftRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface BillRepository extends SoftRepository<Bill> {

    @Query("select b from Bill b where b.idBill = :id and b.deleted = false")
    Bill findById(Integer id);

    @Query("select b from Bill b " +
            "where b.deleted = false " +
            "and b.dateIn >= :dateIn and b.dateOut <= :dateOut " +
            "order by b.idBill desc")
    Page<Bill> lastFindAll(Pageable pageable, @Param("dateIn") LocalDateTime dateIn, @Param("dateOut") LocalDateTime dateOut);

    @Query("select count(b) from Bill b " +
            "where b.deleted = false " +
            "and b.dateIn >= :dateIn and b.dateOut <= :dateOut")
    Integer findAmount(@Param("dateIn") LocalDateTime dateIn, @Param("dateOut") LocalDateTime dateOut);

    @Query("select sum(b.total) from Bill b " +
            "where b.deleted = false " +
            "and b.dateIn >= :dateIn and b.dateOut <= :dateOut")
    Double findProfits(@Param("dateIn") LocalDateTime dateIn, @Param("dateOut") LocalDateTime dateOut);

    @Query("select b from Bill b " +
            "where b.deleted = false " +
            "and b.dateIn >= :dateIn "+
            "and b.total is null")
    List<Bill> findVehicleIn(@Param("dateIn") LocalDateTime dateIn);

}
