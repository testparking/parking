package com.test.parking.repositories;

import com.test.parking.domains.Lot;
import com.test.parking.repositories.commons.SoftRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LotRepository extends SoftRepository<Lot> {

    @Query("select l from Lot l where l.idLot = :id and l.deleted = false")
    Lot findById(Integer id);
}
