package com.test.parking.repositories;

import com.test.parking.domains.Lot;
import com.test.parking.domains.Status;
import com.test.parking.repositories.commons.SoftRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends SoftRepository<Status> {

    @Query("select s from Status s where s.id_Status = :id and s.deleted = false")
    Status findById(Integer id);

}
