package com.test.parking.repositories;

import com.test.parking.domains.Bill;
import com.test.parking.domains.VehicleBill;
import com.test.parking.repositories.commons.SoftRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleBillRepository extends SoftRepository<VehicleBill> {

    @Query("select vb from VehicleBill vb where vb.bill = :id and vb.deleted = false")
    VehicleBill findByBill(Bill id);

}
