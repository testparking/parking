package com.test.parking.repositories;

import com.test.parking.domains.Vehicle;
import com.test.parking.repositories.commons.SoftRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends SoftRepository<Vehicle> {
}
