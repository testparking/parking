package com.test.parking.repositories;


import com.test.parking.domains.VehicleType;
import com.test.parking.repositories.commons.SoftRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleTypeRepository extends SoftRepository<VehicleType> {

    @Query("select v from VehicleType v where v.idVehicleType = :id and v.deleted = false")
    VehicleType findById(Integer id);

}
