package com.test.parking.repositories.commons;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.UUID;

@NoRepositoryBean
public interface SoftRepository <T> extends Repository<T, Long>{

    @Query("select e from #{#entityName} e where e.deleted=false")
    List<T> findAll();

    @Query("select e from #{#entityName} e where e.deleted=false")
    Page<T> findAll(Pageable pageable);

    @Query("select e from #{#entityName} e where e.deleted=true")
    List<T> recycleBin();

    @Modifying
    @Query("update #{#entityName} e set e.deleted=true where e.id=?1")
    void softDelete(Integer id);

    @Modifying
    @Query("update #{#entityName} e set e.deleted=true where e=?1")
    void delete(T entity, UUID deletedId);

    @Modifying
    @Query("update #{#entityName} e set e.deleted=true where e=?1")
    void  softDelete(T entity, UUID deletedId);

    T save(T entity);

}
