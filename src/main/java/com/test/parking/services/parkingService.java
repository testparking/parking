package com.test.parking.services;


import com.test.parking.controllers.request.entranceRequest;
import com.test.parking.controllers.request.updateRequest;
import com.test.parking.controllers.response.entranceResponse;
import com.test.parking.controllers.response.outResponse;
import com.test.parking.controllers.response.profitResponse;
import com.test.parking.domains.*;
import com.test.parking.repositories.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class parkingService {

    private final VehicleTypeRepository repository;
    private final VehicleRepository vehicleRepository;
    private final BillRepository billRepository;
    private final VehicleBillRepository vehicleBillRepository;
    private final LotRepository lotRepository;
    private final StatusRepository statusRepository;

    public parkingService(VehicleTypeRepository repository, VehicleRepository vehicleRepository, BillRepository billRepository, VehicleBillRepository vehicleBillRepository, LotRepository lotRepository, StatusRepository statusRepository) {
        this.repository = repository;
        this.vehicleRepository = vehicleRepository;
        this.billRepository = billRepository;
        this.vehicleBillRepository = vehicleBillRepository;
        this.lotRepository = lotRepository;
        this.statusRepository = statusRepository;
    }

    @Transactional
    public entranceResponse entrance (entranceRequest request) throws Exception {

        Lot lot = lotRepository.findById(request.getLot());

        if(lot.getStatus().getId_Status() == 3){

            VehicleType vehicleType = repository.findById(request.getVehicleType());
            if(vehicleType != null){
                if(lot.getVehicleType() == vehicleType){

                    Vehicle vehicle = new Vehicle();
                    VehicleBill vehicleBill = new VehicleBill();
                    Bill bill = new Bill();

                    vehicle.setPlate(request.getPlate());
                    vehicle.setVehicleClass(request.getVehicleClass());
                    vehicle.setVehicleType(vehicleType);
                    vehicle = vehicleRepository.save(vehicle);

                    Status status = statusRepository.findById(4);
                    lot.setStatus(status);
                    lotRepository.save(lot);

                    bill.setLot(lot);
                    bill.setDateIn(LocalDateTime.now());
                    bill = billRepository.save(bill);

                    vehicleBill.setVehicle(vehicle);
                    vehicleBill.setBill(bill);
                    vehicleBillRepository.save(vehicleBill);

                    return new entranceResponse(vehicle, bill);

                }else{

                    throw new Exception("Lot is not available to this vehicle type");

                }
            }else{
                throw new Exception("Vehiclo Type invalid");
            }
        } else {

            throw new Exception("Lot is busy");

        }
    }

    @Transactional
    public outResponse out (Integer idBill) throws Exception {

        Bill bill = billRepository.findById(idBill);
        if(bill != null){
            double dis;
            double tot;
            Lot lot = bill.getLot();
            VehicleBill vehicleBill = vehicleBillRepository.findByBill(bill);

            bill.setDateOut(LocalDateTime.now());
            var dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:s");
            String date = LocalDateTime.now().format(dateFormat);
            String dateIn = vehicleBill.getBill().getDateIn().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:s"));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");

            Date d = format.parse(date);
            Date cd = format.parse(dateIn);

            long diff = d.getTime() - cd.getTime();
            TimeUnit time = TimeUnit.HOURS;
            long difference = time.convert(diff, TimeUnit.MILLISECONDS);

            if (Objects.equals(vehicleBill.getVehicle().getVehicleType().getNameVehicle(), "Motocicletas")){
                tot = (double)(difference+1)*62;
            }else{
                tot = (double)(difference+1)*120;
            }

            if (Objects.equals(vehicleBill.getVehicle().getVehicleClass(), "Hibrido") || Objects.equals(vehicleBill.getVehicle().getVehicleClass(), "Electrico")){
                dis = tot*0.25;
            } else{
                dis = 0.0;
            }

            tot = tot - dis;

            bill.setDiscount(dis);
            bill.setTotal(tot);
            bill = billRepository.save(bill);

            Status status = statusRepository.findById(3);
            lot.setStatus(status);
            lotRepository.save(lot);

            return new outResponse(vehicleBill, bill);
        }else{
            throw new Exception("The bill does not exist");
        }
    }

    @Transactional
    public Page<outResponse> getAll(PageRequest requestPage, LocalDateTime dateIn, LocalDateTime dateOut){
        Page<Bill> bills = billRepository.lastFindAll(requestPage,dateIn, dateOut);
        return bills.map(it -> new outResponse(vehicleBillRepository.findByBill(it), it));
    }

    @Transactional
    public outResponse getById (Integer idBill){
        Bill bill = billRepository.findById(idBill);
        VehicleBill vehicleBill = vehicleBillRepository.findByBill(bill);

        return new outResponse(vehicleBill, bill);
    }

    @Transactional
    public profitResponse profits(LocalDateTime dateIn, LocalDateTime dateOut){

        List<Bill> bills = billRepository.findVehicleIn(dateIn);
        if (bills != null){
            bills.forEach(it-> {
                try {
                    out(it.getIdBill());
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
        }

        Integer amount = billRepository.findAmount(dateIn,dateOut);
        Double profits = billRepository.findProfits(dateIn,dateOut);

        return new profitResponse(amount, profits);

    }

    @Transactional
    public outResponse update(Integer idBill, updateRequest request){

        Bill bill = billRepository.findById(idBill);
        VehicleBill vehicleBill = vehicleBillRepository.findByBill(bill);
        Vehicle vehicle = vehicleBill.getVehicle();

        VehicleType vehicleType = repository.findById(request.getVehicleType());
        vehicle.setVehicleType(vehicleType);
        vehicle.setVehicleClass(request.getVehicleClass());
        vehicleRepository.save(vehicle);

        LocalDateTime dateIn = LocalDateTime.parse(request.getDateIn());
        LocalDateTime dateOut = LocalDateTime.parse(request.getDateOut());

        bill.setDateIn(dateIn);
        bill.setDateOut(dateOut);
        bill = billRepository.save(bill);

        return new outResponse(vehicleBill, bill);

    }

    @Transactional
    public void delete(Integer idBill){

        Bill bill = billRepository.findById(idBill);
        VehicleBill vehicleBill = vehicleBillRepository.findByBill(bill);

        billRepository.softDelete(idBill);
        vehicleBillRepository.softDelete(vehicleBill.getIdVehicleBill());
        vehicleRepository.softDelete(vehicleBill.getVehicle().getIdVehicle());

    }

}
